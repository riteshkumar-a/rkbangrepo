package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.StudentDetailsdto;
import com.example.demo.dto.Studentdto;
import com.example.demo.entity.Student;

public interface StudentService {
	public String CreateStudent(Studentdto studentdto);
	public String login(StudentDetailsdto studetailsdto);
	public String updateStd(StudentDetailsdto studetailsdto);
	public List<Student> getStudents();
	List<Student> getStudents(Student stu);

}
