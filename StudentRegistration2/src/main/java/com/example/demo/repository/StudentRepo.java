package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.Studentdto;
import com.example.demo.entity.Student;

@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {

	String save(Studentdto studentdto);

	Student findByName(String username);

	List<Student> findAll(Student stu);
	
	

	

}
