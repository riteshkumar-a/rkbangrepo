package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Student;

public interface UserRepository extends JpaRepository<Student, Integer> {
	Student findByUsername(String username);

}
