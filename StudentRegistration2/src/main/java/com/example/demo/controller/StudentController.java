package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.StudentDetailsdto;
import com.example.demo.dto.Studentdto;
import com.example.demo.entity.Student;
import com.example.demo.service.StudentService;

@RestController
public class StudentController {
	
	@Autowired
	StudentService studentservice;
	
	@PostMapping("/addStudent")
	public String createStudent(@RequestBody Studentdto studentdto)
	{
		return studentservice.CreateStudent(studentdto);
	}
	@PutMapping("/login")
	public String updateLogin(@RequestBody StudentDetailsdto studetailsdto)
	{
		return studentservice.login(studetailsdto);
	}
	@PutMapping
	public String updateStudent(@RequestBody StudentDetailsdto studetailsdto )
	{
		return studentservice.updateStd(studetailsdto);
		
	}
	public List<Student> findAllStudents()
	{
		return studentservice.getStudents();
		
	}
	
	public void delete()
	{
		System.out.println("delete students");
	}
	

}
