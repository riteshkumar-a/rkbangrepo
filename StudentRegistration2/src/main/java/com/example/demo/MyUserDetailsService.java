package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.example.demo.entity.Student;

public class MyUserDetailsService implements UserDetailsService {
	@Autowired
     private UserRepository repo;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Student stu = repo.findByUsername(username);
		if(stu==null)
		{
			throw new UsernameNotFoundException("user not found");
		}
		return new UserPrinciple(stu);
	}

}
