package com.petpeers.petpeers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.petpeers.petpeers.dto.ApiResponse;
import com.petpeers.petpeers.dto.BuyPetRequestDto;
import com.petpeers.petpeers.dto.LoginRequestDto;
import com.petpeers.petpeers.dto.PetDto;
import com.petpeers.petpeers.dto.PetListResponseDto;
import com.petpeers.petpeers.dto.UserRequestDto;
import com.petpeers.petpeers.service.IPetPeersService;
import com.petpeers.petpeers.service.IUserService;

@RestController
public class PetPeersController {

	@Autowired
	IUserService iUserService;

	@Autowired
	IPetPeersService iPetPeersService;

	@PostMapping("/addUser")
	public ResponseEntity<ApiResponse> addUser(@RequestBody UserRequestDto userRequestDto) {
		return iUserService.addUser(userRequestDto);
	}

	@PostMapping("/userLogin")
	public ResponseEntity<ApiResponse> userLogin(@RequestBody LoginRequestDto loginRequestDto) {
		return iUserService.userLogin(loginRequestDto);
	}

	@PostMapping("/addPet")
	public ResponseEntity<ApiResponse> addPet(@RequestBody PetDto petDto) {
		return iPetPeersService.addPet(petDto);
	}

	@GetMapping("/getAllPets")
	public ResponseEntity<PetListResponseDto> allPets() {
		return iPetPeersService.allPets();
	}

	@PostMapping("/buyPet")
	public ResponseEntity<ApiResponse> buyPet(@RequestBody BuyPetRequestDto buyPetRequestDto) {
		return iPetPeersService.buyPet(buyPetRequestDto);
	}

}
