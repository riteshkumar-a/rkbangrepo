package com.petpeers.petpeers.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petpeers.petpeers.model.Pet;

public interface PetPeersRepository extends JpaRepository<Pet, Long>{
	
	List<Pet> findByPetOwnerId();
	

}
