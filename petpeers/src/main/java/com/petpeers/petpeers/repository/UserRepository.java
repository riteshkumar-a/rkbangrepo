package com.petpeers.petpeers.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petpeers.petpeers.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByUserName(String userName);
	User findByUserNameAndPassword(String userName,String password);

}
