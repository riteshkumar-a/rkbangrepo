package com.petpeers.petpeers.dto;

import java.io.Serializable;

public class PetListDto implements Serializable{
	private static final long serialVersionUID = 1L;

	public PetListDto() {
	}

	private Long petId;
	private String petName;
	private int petAge;
	private String petPlace;
	private String petAction;

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public int getPetAge() {
		return petAge;
	}

	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}

	public String getPetAction() {
		return petAction;
	}

	public void setPetAction(String petAction) {
		this.petAction = petAction;
	}

	public Long getPetId() {
		return petId;
	}

	public void setPetId(Long petId) {
		this.petId = petId;
	}

}
