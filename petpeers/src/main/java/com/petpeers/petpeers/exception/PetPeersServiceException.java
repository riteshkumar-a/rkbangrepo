package com.petpeers.petpeers.exception;

public class PetPeersServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public PetPeersServiceException() {
		super();
	}

	public PetPeersServiceException(String message) {
		super(message);
	}

}
