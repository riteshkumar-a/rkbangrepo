package com.petpeers.petpeers.service;

import org.springframework.http.ResponseEntity;

import com.petpeers.petpeers.dto.ApiResponse;
import com.petpeers.petpeers.dto.BuyPetRequestDto;
import com.petpeers.petpeers.dto.PetDto;
import com.petpeers.petpeers.dto.PetListResponseDto;

public interface IPetPeersService {
	
	ResponseEntity<ApiResponse> addPet(PetDto petDto);
	
	ResponseEntity<PetListResponseDto> allPets();
	
	ResponseEntity<ApiResponse> buyPet(BuyPetRequestDto buyPetRequestDto);
	
	ResponseEntity<PetListResponseDto> myPets(Long ownerId);
}
