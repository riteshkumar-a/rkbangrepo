package com.petpeers.petpeers.service;

import org.springframework.http.ResponseEntity;

import com.petpeers.petpeers.dto.ApiResponse;
import com.petpeers.petpeers.dto.LoginRequestDto;
import com.petpeers.petpeers.dto.UserRequestDto;

public interface IUserService {
	
	ResponseEntity<ApiResponse> addUser(UserRequestDto userRequestDto);
	
	ResponseEntity<ApiResponse> userLogin(LoginRequestDto loginRequestDto);

}
