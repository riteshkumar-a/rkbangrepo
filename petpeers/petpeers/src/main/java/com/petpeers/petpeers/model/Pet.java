package com.petpeers.petpeers.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PET")
public class Pet {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "PET_NAME")
	private String petName;

	@Column(name = "PET_AGE")
	private int petAge;

	@Column(name = "PET_PLACE")
	private String petPlace;

	@Column(name = "PET_OWNERID")
	private Long petOwnerId;

	@Column(name = "PET_ACTION")
	private boolean action;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Pet() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public int getPetAge() {
		return petAge;
	}

	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}

	public Long getPetOwnerId() {
		return petOwnerId;
	}

	public void setPetOwnerId(Long petOwnerId) {
		this.petOwnerId = petOwnerId;
	}

	public boolean isAction() {
		return action;
	}

	public void setAction(boolean action) {
		this.action = action;
	}

}
