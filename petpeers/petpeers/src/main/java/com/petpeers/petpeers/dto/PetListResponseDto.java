package com.petpeers.petpeers.dto;

import java.io.Serializable;
import java.util.List;

public class PetListResponseDto extends ApiResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<PetListDto> petLists;

	public List<PetListDto> getPetLists() {
		return petLists;
	}

	public void setPetLists(List<PetListDto> petLists) {
		this.petLists = petLists;
	}
}
