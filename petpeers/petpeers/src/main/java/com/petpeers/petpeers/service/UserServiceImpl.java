package com.petpeers.petpeers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.petpeers.petpeers.dto.ApiResponse;
import com.petpeers.petpeers.dto.LoginRequestDto;
import com.petpeers.petpeers.dto.UserRequestDto;
import com.petpeers.petpeers.model.User;
import com.petpeers.petpeers.repository.UserRepository;

@Service
public class UserServiceImpl implements IUserService {

	private static final String FAILURE = "Failure";
	private static final String SUCCESS = "Success";

	@Autowired
	UserRepository userRepository;

	@Override
	public ResponseEntity<ApiResponse> addUser(UserRequestDto userRequestDto) {
		User user = null;
		try {
			if (userRequestDto != null) {

				if (userRequestDto.getPassword().equals(userRequestDto.getConfirmPassword())) {
					user = new User();
					user.setUserName(userRequestDto.getUserName());
					user.setPassword(userRequestDto.getConfirmPassword());
					userRepository.save(user);
					return ResponseEntity.status(HttpStatus.CREATED)
							.body(new ApiResponse("Registration successfull", SUCCESS, 200));
				} else {
					return ResponseEntity.status(HttpStatus.CREATED)
							.body(new ApiResponse("Password and confirm password should be same", FAILURE, 404));

				}

			}

		} catch (Exception e) {

			return ResponseEntity.status(404).body(new ApiResponse(e.getMessage(), FAILURE, 404));
		}

		return null;
	}

	@Override
	public ResponseEntity<ApiResponse> userLogin(LoginRequestDto loginRequestDto) {
		try {
			if (loginRequestDto != null) {
				User user = userRepository.findByUserNameAndPassword(loginRequestDto.getUserName(),
						loginRequestDto.getPassword());
				if (user != null) {
					if (loginRequestDto.getUserName().equals(user.getUserName())
							&& loginRequestDto.getPassword().equals(user.getPassword())) {
						return ResponseEntity.status(HttpStatus.CREATED)
								.body(new ApiResponse("Login successfull", SUCCESS, 200));
					} else {
						return ResponseEntity.status(404).body(new ApiResponse("Login denied", FAILURE, 404));
					}
				} else {
					return ResponseEntity.status(404).body(new ApiResponse("Login denied", FAILURE, 404));
				}
			}

		} catch (Exception e) {
			return ResponseEntity.status(404).body(new ApiResponse(e.getMessage(), FAILURE, 404));
		}

		return null;
	}

}
