package com.petpeers.petpeers.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.petpeers.petpeers.dto.ApiResponse;
import com.petpeers.petpeers.dto.BuyPetRequestDto;
import com.petpeers.petpeers.dto.PetDto;
import com.petpeers.petpeers.dto.PetListDto;
import com.petpeers.petpeers.dto.PetListResponseDto;
import com.petpeers.petpeers.model.Pet;
import com.petpeers.petpeers.model.User;
import com.petpeers.petpeers.repository.PetPeersRepository;
import com.petpeers.petpeers.repository.UserRepository;

@Service
public class PetPeersServiceImpl implements IPetPeersService {

	private static final String FAILURE = "Failure";
	private static final String SUCCESS = "Success";

	@Autowired
	PetPeersRepository petPeersRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	public ResponseEntity<ApiResponse> addPet(PetDto petDto) {

		ResponseEntity<ApiResponse> response = null;
		Pet pet = null;
		try {
			if (petDto != null) {

				Optional<User> user = userRepository.findById(petDto.getPetOwnerId());
				if (user.isPresent()) {
					pet = new Pet();
					pet.setPetName(petDto.getPetName());
					pet.setPetAge(petDto.getPetAge());
					pet.setPetPlace(petDto.getPetPlace());
					pet.setPetOwnerId(petDto.getPetOwnerId());
					pet.setAction(false);
					petPeersRepository.save(pet);
					response = ResponseEntity.status(404).body(new ApiResponse("Pet added succesfully", SUCCESS, 200));
				} else {
					response = ResponseEntity.status(404)
							.body(new ApiResponse("Please enter correct owner id", SUCCESS, 200));
				}

			} else {
				response = ResponseEntity.status(404).body(new ApiResponse("Not added", SUCCESS, 200));
			}

		} catch (Exception e) {
			response = ResponseEntity.status(404).body(new ApiResponse(e.getMessage(), FAILURE, 200));
		}
		return response;

	}

	@Override
	public ResponseEntity<PetListResponseDto> allPets() {
		List<Pet> petLists = petPeersRepository.findAll();
		List<PetListDto> petListResposneDtoLists = new ArrayList<>();
		PetListResponseDto petListResponseDto = new PetListResponseDto();
		if (petLists != null && !petLists.isEmpty()) {
			petLists.stream().forEach(pet -> {
				PetListDto petListResposneDto = new PetListDto();
				petListResposneDto.setPetId(pet.getId());
				petListResposneDto.setPetName(pet.getPetName());
				petListResposneDto.setPetAge(pet.getPetAge());
				petListResposneDto.setPetPlace(pet.getPetPlace());
				if (pet.isAction())
					petListResposneDto.setPetAction("Sold out");
				else
					petListResposneDto.setPetAction("Available");

				petListResposneDtoLists.add(petListResposneDto);
				petListResponseDto.setPetLists(petListResposneDtoLists);
				petListResponseDto.setMessage("Feteched successfully");
				petListResponseDto.setStatus(SUCCESS);
				petListResponseDto.setStatusCode(200);
			});
			return ResponseEntity.status(200).body(petListResponseDto);
		} else {
			petListResponseDto.setMessage("Something went wrong");
			petListResponseDto.setStatus(FAILURE);
			petListResponseDto.setStatusCode(401);
			return ResponseEntity.status(401).body(petListResponseDto);
		}

	}

	@Override
	public ResponseEntity<ApiResponse> buyPet(BuyPetRequestDto buyPetRequestDto) {

		ResponseEntity<ApiResponse> response = null;
		try {
			if (buyPetRequestDto != null) {
				Optional<Pet> pet = petPeersRepository.findById(buyPetRequestDto.getPetId());
				if (pet.isPresent()) {
					pet.get().setAction(true);
					petPeersRepository.save(pet.get());
					response = ResponseEntity.status(401)
							.body(new ApiResponse("Pet purchased successfully", SUCCESS, 200));
				} else
					response = ResponseEntity.status(401).body(new ApiResponse("Pet is not available", FAILURE, 401));

			}

		} catch (Exception e) {
			response = ResponseEntity.status(401).body(new ApiResponse(e.getMessage(), FAILURE, 401));
		}

		return response;
	}
	
	@Override
	public ResponseEntity<PetListResponseDto> myPets(Long ownerId) {
		List<Pet> petLists = petPeersRepository.findByPetOwnerId();
		List<PetListDto> petListResposneDtoLists = new ArrayList<>();
		PetListResponseDto petListResponseDto = new PetListResponseDto();
		if (petLists != null && !petLists.isEmpty()) {
			petLists.stream().forEach(pet -> {
				PetListDto petListResposneDto = new PetListDto();
				petListResposneDto.setPetId(pet.getId());
				petListResposneDto.setPetName(pet.getPetName());
				petListResposneDto.setPetAge(pet.getPetAge());
				petListResposneDto.setPetPlace(pet.getPetPlace());
				if (pet.isAction())
					petListResposneDto.setPetAction("Sold out");
				else
					petListResposneDto.setPetAction("Available");

				petListResposneDtoLists.add(petListResposneDto);
				petListResponseDto.setPetLists(petListResposneDtoLists);
				petListResponseDto.setMessage("Feteched successfully");
				petListResponseDto.setStatus(SUCCESS);
				petListResponseDto.setStatusCode(200);
			});
			return ResponseEntity.status(200).body(petListResponseDto);
		} else {
			petListResponseDto.setMessage("Something went wrong");
			petListResponseDto.setStatus(FAILURE);
			petListResponseDto.setStatusCode(401);
			return ResponseEntity.status(401).body(petListResponseDto);
		}

	}

}
