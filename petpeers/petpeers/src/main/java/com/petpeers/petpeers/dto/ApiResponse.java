package com.petpeers.petpeers.dto;

import java.io.Serializable;

public class ApiResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String status;
	private Integer statusCode;
	private String message;
	
	public ApiResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public ApiResponse(String message,String status, Integer statusCode) {
		super();
		this.status = status;
		this.statusCode = statusCode;
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
