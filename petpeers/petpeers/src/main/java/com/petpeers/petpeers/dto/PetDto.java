package com.petpeers.petpeers.dto;

public class PetDto {

	private String petName;

	private int petAge;

	private String petPlace;

	private Long petOwnerId;

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public int getPetAge() {
		return petAge;
	}

	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}

	public Long getPetOwnerId() {
		return petOwnerId;
	}

	public void setPetOwnerId(Long petOwnerId) {
		this.petOwnerId = petOwnerId;
	}
	
	
}
